import { Component, OnInit, Input, Output } from '@angular/core';
import { People } from '../models/people';
import { DataService } from '../services/data.service';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  people: People[];
  persons: People[];
  filteredPerson: any[];
  person;

  height = "55px";

  constructor(private dataService: DataService) {
    this.dataService.getPeople().subscribe(data => {
      this.people = this.filteredPerson = data.People;
    });


  }

  ngOnInit() {
  }

  onPersonChosen(value) {
    this.person = this.onFiltering(value);
  }

  onChosenUserFromSidebar(value) {
    let heightAdjustment = '115px';
    this.person = value;
    this.height = heightAdjustment;
  }


  onFiltering(query) {
    this.filteredPerson = (query) ?
      this.people.filter(p => p.name.toLowerCase().includes(query.toLowerCase())) :
      this.people;

    if (this.filteredPerson.length === 1) {
      return this.filteredPerson[0];
    }
  }

}
