import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { People } from '../models/people';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input('people') people: People[];
  @Output() chosenUserFromSidebar = new EventEmitter();


  constructor() {

  }

  onClick(value: any) {
    this.chosenUserFromSidebar.emit(value);
  }


}
