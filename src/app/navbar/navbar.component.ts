import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { People } from '../models/people';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input('person') person: People;
  @Output() filtering = new EventEmitter();
  @Output() choosePersonFromSearch = new EventEmitter();

  isCollapse;

  /** icons */
  searchIcon = faSearch;
  clearIcon = faTimes;

  constructor() {
  }

  ngOnInit() {
  }

  choosePerson(search) {
    this.choosePersonFromSearch.emit(search.value);
    search.value = '';
  }
  
  filter(query) {
    this.filtering.emit(query.value);
  }

  removeSearch(search) {
    search.value = '';
    this.filtering.emit(search.value);

  }


}
