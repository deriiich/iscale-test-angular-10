import { NgModule } from "@angular/core";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as icons from '@fortawesome/free-solid-svg-icons';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
@NgModule({

    exports: [
        FontAwesomeModule,

    ]
})
export class FontAwesomeIconModule { 
    faCoffee = faCoffee;
}