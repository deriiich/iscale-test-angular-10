export interface People {
    name: string;
    rating: number;
    img: string;
    Description: any;
    Likes: string[];
    Dislikes: string[];
}
